package ee.bcs.valiit;

import java.util.Scanner;

public class Ex2 {
    public static void main(String[] args) {
        String text="Rida: See on esimene rida. Rida: See on teine rida. Rida: See on kolmas rida.";
        Scanner sc=new Scanner(text);
        sc.useDelimiter("Rida: ");
        while(sc.hasNext()){
            System.out.println(sc.next());
        }
    }
}
